﻿#pragma once

#include <Windows.h>
#include <stdint.h>
#include <string>
#include <functional>

namespace tlbox { namespace misc
{
	enum WeakSymbol
	{
		Default
	};

	class Noncopyable
	{
	protected:
		Noncopyable() {}
		~Noncopyable() {}

	private:
		Noncopyable( __in const Noncopyable & other );
		Noncopyable & operator = ( __in const Noncopyable & other );
	};
	
	class ScopedHandle
		: private Noncopyable
	{
	public:
		ScopedHandle( HANDLE handle ) : handle_( handle ) {}
		~ScopedHandle()
		{
			CloseHandle( handle_ );
		}

		operator HANDLE() const
		{
			return handle_;
		}

	private:
		HANDLE handle_;
	};

	const size_t kGoldenRatioPrime = 0x9e370001;	// (2^31) + (2^29) - (2^25) + (2^22) - (2^19) - (2^16) + 1;

	inline size_t hash( int key )
	{
		return key * kGoldenRatioPrime;
	}

	inline size_t hash( unsigned int key ) 
	{
		return key * kGoldenRatioPrime;
	}

	inline size_t hash( long key )
	{
		return key * kGoldenRatioPrime;
	}

	inline size_t hash( const char * key )
	{
		return hash( reinterpret_cast< long >( key ) );
	}

	inline size_t hash( const std::string & key )
	{
		size_t h = kGoldenRatioPrime;
		
		for( const char * c = key.c_str(); *c != 0; ++ c )
		{
			h = ( h * 33 ) + *c;
		}

		return h;
	}

	template < class KeyTy >
	class HashFunctor
		: public std::unary_function< KeyTy, size_t >
	{
	public:
		size_t operator()( const KeyTy & k ) const 
		{
			return hash( k );
		}
	};

	template < class KeyTy >
	class EqFunctor
		: public std::binary_function< KeyTy, KeyTy, bool >
	{
	public:
		bool operator()( const KeyTy & k1, const KeyTy & k2 ) const 
		{
			return k1 == k2;
		}
	};

} }
