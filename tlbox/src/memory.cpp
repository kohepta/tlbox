﻿#include <assert.h>
#include "../misc.h"
#include "../singleton.h"
#include "../memory.h"

namespace tlbox { namespace memory { namespace
{
	class MemoryPool
		: private tlbox::misc::Noncopyable
	{
	private:
		struct BlockList
		{
			BlockList * next;
		};

		struct Header
		{
			Header * next;
			uint32_t flags;
			size_t size;
		};

	public:
		void * allocate( __in size_t size );
		void release( __in void * buffer );

		static bool CreateInstance(
			__in size_t allocSize, 
			__in size_t initialAllocSize = 0,
			__in uint32_t maxAllocCount = 0 )
		{
			try
			{
				tlbox::singleton::Singleton< MemoryPool >::Initialize( allocSize, initialAllocSize, maxAllocCount );
				
				return true;
			}
			catch ( ... )
			{
			}

			return false;
		}

		static void DeleteInstance()
		{
			tlbox::singleton::Singleton< MemoryPool >::Finalize();
		}

		static MemoryPool * GetInstance()
		{
			return tlbox::singleton::Singleton< MemoryPool >::Get();
		}

	private:
		explicit MemoryPool::MemoryPool(
			__in size_t allocSize, 
			__in size_t initialAllocSize = 0,
			__in uint32_t maxAllocCount = 0 );
		~MemoryPool();

		inline Header * nextHeader( __in const Header * header ) const
		{
			return reinterpret_cast< Header * >( ( size_t )( header + 1 ) + header->size );
		}

		bool resetBlock();

		Header * divideBlock( __in size_t size );

		Header * findBlock( Header * target );

		void connectBlock( Header * target );

		Header * openBlock();

		Header * addBlock();

		void closeBlock();

	private:
		size_t allocCount_;
		BlockList * blockList_;
		Header * freeList_;

		const size_t kHeaderSize;
		const size_t kAllocSize;
		const size_t kInitialAllocSize;
		const uint32_t kAllocKey;
		const uint32_t kMaxAllocCount;

#if __TLBOX_MEMORY_TEST__
		friend class tlbox::memory::test::MemoryPoolTest;
#endif // __TLBOX_MEMORY_TEST__

		friend class tlbox::singleton::Singleton< MemoryPool >;
	};

	template < size_t Size >
	inline size_t Align( __in size_t d )
	{
		return ( d + ( Size - 1 ) ) & ~( Size - 1 );
	}

} } }

namespace tlbox { namespace memory { namespace
{
	MemoryPool::MemoryPool(
		__in size_t allocSize, 
		__in size_t initialAllocSize /* = 0 */,
		__in uint32_t maxAllocCount /* = 0 */ )
		: allocCount_( 0 )
		, blockList_( NULL )
		, freeList_( NULL )
		, kHeaderSize( sizeof( Header ) )
		, kAllocSize( allocSize )
		, kInitialAllocSize( initialAllocSize < allocSize ? allocSize : initialAllocSize )
		, kAllocKey( 0xCAFEBABE )
		, kMaxAllocCount( maxAllocCount ) {}

	MemoryPool::~MemoryPool()
	{
		closeBlock();
	}

	void * MemoryPool::allocate( __in size_t size )
	{
		size_t requiredSize = Align< sizeof( void * ) >( size );

		if ( 0 >= requiredSize || Align< sizeof( void * ) >( kAllocSize ) < requiredSize )
		{
			return NULL;
		}

		if ( !resetBlock() )
		{
			return NULL;
		}

		Header * available = divideBlock( requiredSize );

		if ( NULL == available )
		{
			return NULL;
		}

		return reinterpret_cast< void * >( available + 1 );
	}

	void MemoryPool::release( __in void * buffer )
	{
		if ( 0 == allocCount_ )
		{
			return;
		}

		if ( NULL != buffer )
		{
			Header * free = reinterpret_cast< Header * >( buffer ) - 1;

			if ( kAllocKey != free->flags )
			{
				// アロケートヘッダーが壊れている。
				return;
			}

			if ( NULL == freeList_ )
			{
				freeList_ = free;
				freeList_->next = freeList_;
			}
			else
			{
				// 解放ブロック位置検索
				freeList_ = findBlock( free );

				// 連結処理
				connectBlock( free );
			}

			// キーをリセット
			free->flags = 0;
		}
	}

	bool MemoryPool::resetBlock()
	{
		if ( 0 == allocCount_ )
		{
			// 初動時のみ実行
			// kInitialAllocSize 分の領域を kAllocSize 毎に分割して確保する
			uint32_t count = ( kInitialAllocSize + ( kAllocSize - 1 ) ) / kAllocSize;
			
			for ( uint32_t i = 0; i < count; i ++ )
			{
				if ( NULL == addBlock() )
				{
					return false;
				}
			}
		}
		else if ( NULL == freeList_ )
		{
			// MemoryPool::divideBlock 処理によって、
			// freeList_ が NULL になることは起こり得る

			if ( NULL == ( freeList_ = openBlock() ) )
			{
				return false;
			}
		}

		return true;
	}

	MemoryPool::Header * MemoryPool::divideBlock( __in size_t size )
	{
		for ( Header * p = freeList_, * n = p->next; ; p = n, n = n->next )
		{
			// 使用可能なブロックを発見
			if ( n->size >= size )
			{
				// 残りブロックサイズがヘッダーサイズ未満
				// n->size == size のケースも含まれる
				if ( n->size - size < kHeaderSize )
				{
					if ( p->next != n->next )
					{
						// --- p --- n ---
						// --- p --- n --- nn ---
						// --- p --- n -- nn --- nnn ---
						// というように少なくとも２つ以上ブロックがある状態の場合で n を取得. 
						// このとき freeList_ が n を指している可能性があるので、
						// freeList_ を p に指しなおす
						freeList_ = p;

						// n をリストから削除
						p->next = n->next;
					}
					else
					{
						// 唯一のブロックを取得
						freeList_ = NULL;
					}
				}
				else
				{
					// ブロックサイズが要求サイズより大きいので分割
					// 分割した後方部分をユーザーに渡す
					n->size -= kHeaderSize + size;
					n = nextHeader( n );
					n->size = size;
				}

				// アロケートキーを設定
				n->flags = kAllocKey;

				return n;
			}

			if ( n == freeList_ )
			{
				// 一周した
				if ( NULL == ( n = addBlock() ) )
				{
					break;
				}
			}
		}

		return NULL;
	}

	MemoryPool::Header * MemoryPool::findBlock( Header * target )
	{
		Header * found = freeList_;
		
		do
		{
			// | ----- fouud ------- target -------- found->next ---- |
			if ( found < found->next && ( found < target && target < found->next ) )
			{
				break;
			}

			// | -------- found->next | found ---------------- target |
			// | ----- target ----- found->next --------------- found |
			if ( found >= found->next && ( found < target || target < found->next ) )
			{
				break;
			}
				
			found = found->next;
		}
		while ( found != freeList_ );

		return found;
	}

	void MemoryPool::connectBlock( Header * target )
	{
		if ( nextHeader( target ) == freeList_->next )
		{
			// | ------- found ------ target + found->next ----- |
			// | ------- target + found->next ------------ found |
			target->size += freeList_->next->size + kHeaderSize;
			
			if ( freeList_->next == freeList_ )
			{
				// freeList_ が単独で存在する場合、
				// target を freeList_ として扱う
				freeList_ = target;
			}
			else
			{
				target->next = freeList_->next->next;
			}
		}
		else
		{
			// | --- found ---- target ----- found->next ------- |
			// | ------- target ----- found->next -------- found |
			target->next = freeList_->next;
		}

		if ( nextHeader( freeList_ ) == target )
		{
			// | -------- found + target ------ found->next ---- |
			// | ------- found->next ------------ found + target |
			freeList_->size += target->size + kHeaderSize;
			freeList_->next = target->next;
		}
		else
		{
			// | ------- found ---- target ----- found->next ----|
			// | ---- found->next --------- found ------- target |
			freeList_->next = target;
		}
	}

	MemoryPool::Header * MemoryPool::openBlock()
	{
		if ( 0 < kMaxAllocCount && kMaxAllocCount <= allocCount_ )
		{
			return NULL;
		}

		allocCount_ ++;

		size_t requiredSize = Align< sizeof( void * ) >( kAllocSize );
		BlockList * * tail = &blockList_;

		for ( ; *tail; tail = &(*tail)->next );

		*tail = reinterpret_cast < BlockList  * >(
			::VirtualAlloc(
				NULL,
				requiredSize + kHeaderSize + sizeof( BlockList ), 
				MEM_COMMIT | MEM_RESERVE | MEM_TOP_DOWN,
				PAGE_EXECUTE_READWRITE ) );
			
		if ( NULL == *tail )
		{
			return NULL;
		}

		(*tail)->next = NULL;

		Header * header = reinterpret_cast< Header * >( *tail + sizeof( BlockList ) );

		header->next = header;
		header->flags = 0;
		header->size = requiredSize;

		return header;
	}

	MemoryPool::Header * MemoryPool::addBlock()
	{
		Header * header = openBlock();

		if ( NULL == header )
		{
			return NULL;
		}

		header->flags = kAllocKey;

		this->release( header + 1 );

		return freeList_;
	}

	void MemoryPool::closeBlock()
	{
		for ( BlockList * it = blockList_; it; )
		{
			BlockList * next = it->next;
			
			::VirtualFree( it, 0, MEM_RELEASE );

			it = next;
		}

		allocCount_ = 0;
		blockList_ = NULL;
		freeList_ = NULL;
	}

} } }

namespace tlbox { namespace memory
{
	bool Initialize(
		__in size_t allocSize, 
		__in size_t initialAllocSize /* = 0 */,
		__in uint32_t maxAllocCount /* = 0 */ )
	{
#if __TLBOX_MEMORY_TEST__
		return MemoryPool::CreateInstance( 0x40000, 0, 3 );
#else
		return MemoryPool::CreateInstance( allocSize, initialAllocSize, maxAllocCount );
#endif
	}

	void Finalize()
	{
		MemoryPool::DeleteInstance();
	}

	void * AllocatePool( __in size_t size )
	{
		if ( NULL == MemoryPool::GetInstance() )
		{
			return NULL;
		}

		return MemoryPool::GetInstance()->allocate( size );
	}

	void ReleasePool( __in void * buffer )
	{
		if ( NULL == MemoryPool::GetInstance() )
		{
			return;
		}

		MemoryPool::GetInstance()->release( buffer );
	}

} }

#if __TLBOX_MEMORY_TEST__
namespace tlbox { namespace memory { namespace test
{
#pragma warning( push )
#pragma warning( disable:4996 )
	void MemoryPoolTest::ut1()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		ins->closeBlock();
		assert( NULL == ins->freeList_ );
		assert( NULL == ins->allocate( 0 ) );
		char * str = (char *)ins->allocate( 5 );
		strcpy( str, "hello" );
		assert( !strcmp( "hello", str ) );
		MemoryPool::Header * hdr = (MemoryPool::Header *)str - 1;
		assert( Align< sizeof( void * ) >( 5 ) == hdr->size );
		assert( ins->nextHeader(ins->freeList_) == hdr );
		assert( ins->kAllocSize - Align< sizeof( void * ) >( 5 ) - ins->kHeaderSize == ins->freeList_->size );
		ins->release( str );
		assert( ins->kAllocSize == ins->freeList_->size );
		char * str2 = (char *)ins->allocate( 5 );
		assert( !strcmp( "hello", str2 ) );
		strcpy( str2, "world" );
		assert( !strcmp( "world", str2 ) );
		assert( ins->kAllocSize - Align< sizeof( void * ) >( 5 ) - ins->kHeaderSize == ins->freeList_->size );
		ins->release( str2 );
		assert( ins->kAllocSize  == ins->freeList_->size );
	}

	void MemoryPoolTest::ut2()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		assert( NULL != ins->freeList_ );
		char * str1, * str2;
		str1 = (char *)ins->allocate( 5 );
		strcpy( str1, "hello" );  
		str2 = (char *)ins->allocate( 5 );
		strcpy( str2, "world" );
		assert( !strcmp( "hello", str1 ) );
		assert( !strcmp( "world", str2 ) );
		assert( ins->kAllocSize - 2 * Align< sizeof( void * ) >( 5 ) - 2 * ins->kHeaderSize == ins->freeList_->size );
		ins->release( str1 );
		MemoryPool::Header * hdr1 = (MemoryPool::Header *)str1 - 1;
		assert( ins->freeList_->next == hdr1 );
		assert( hdr1->next == ins->freeList_ );
		assert( ins->kAllocSize - 2 * Align< sizeof( void * ) >( 5 ) - 2 * ins->kHeaderSize == ins->freeList_->size );
		assert( Align< sizeof( void * ) >( 5 ) == hdr1->size );
		ins->release( str2 );
		assert( ins->kAllocSize == ins->freeList_->size );
	}

	void MemoryPoolTest::ut3()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		{
			void * p1, * p2, * p3;
			p1 = ins->allocate(ins->kAllocSize/4 - ins->kHeaderSize );
			p2 = ins->allocate(ins->kAllocSize/4 - ins->kHeaderSize );
			p3 = ins->allocate(ins->kAllocSize/4 - ins->kHeaderSize );
			assert( ins->kAllocSize/4 == ins->freeList_->size );
			ins->release( p2 );
			MemoryPool::Header * old = ins->freeList_;
			ins->release( p1 );
			assert( old->next == ins->freeList_ );
			assert( ins->freeList_->next == old );
			assert( (DWORD)( (MemoryPool::Header*)p2 - 1 ) == (DWORD)ins->freeList_ );
			assert( ins->kAllocSize/2 - ins->kHeaderSize == ins->freeList_->size );
			ins->release( p3 );
			assert( old == ins->freeList_ );
			assert( old->next == old );
			assert( ins->kAllocSize == ins->freeList_->size );
		}

		{
			void * p1, * p2, * p3, * p4;
			p1 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
			p2 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
			p3 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
			p4 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
			//assert( 0 == ins->freeList_->size );
			//MemoryPool::Header * old = ins->freeList_;
			//assert( NULL == ins->freeList_ );
			ins->release( p3 );
			//assert( old == ins->freeList_ );
			//assert( (DWORD)( (MemoryPool::Header *)p3 - 1 ) == (DWORD)ins->freeList_->next );
			//assert( (DWORD)( (MemoryPool::Header *)p3 - 1 )->next == (DWORD)ins->freeList_ );
			ins->release(p1);
			//assert( (DWORD)( (MemoryPool::Header *)p3 - 1 ) == (DWORD)ins->freeList_ );
			//assert( (DWORD)( (MemoryPool::Header *)p1 - 1 ) == (DWORD)ins->freeList_->next );
			//assert( (DWORD)old == (DWORD)ins->freeList_->next->next );
			//assert( ins->kAllocSize/4 - ins->kHeaderSize == ins->freeList_->size );
			ins->release( p4 );
			//assert( old == ins->freeList_ );
			//assert( ins->kAllocSize/2 == ins->freeList_->next->size );
			//assert( ins->kAllocSize/4 - ins->kHeaderSize == ( (MemoryPool::Header *)p1 -1 )->size );
			ins->release( p2 );
			//assert( old == ins->freeList_ );
			assert( ins->kAllocSize == ins->freeList_->size );
		}
	}

	void MemoryPoolTest::ut4()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		void * p1, * p2, * p3, * p4;
		p1 = ins->allocate( 10 );
		p2 = ins->allocate( 20 );
		p3 = ins->allocate( 30 );
		ins->release( p2 );
		p4 = ins->allocate( 20 );
		void * p5 = ins->allocate( 20 );
		assert( p4 == p2 );
		ins->release( p1 );
		ins->release( p3 );
		ins->release( p4 );
		ins->release( p5 );
		assert( ins->kAllocSize == ins->freeList_->size );
	}

	void MemoryPoolTest::ut5()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		void * p1, * p2, * p3, * p4, * p5, * p6, * p7, * p8, * p9;
		void * p10, * p11, * p12, * p13;
		p1  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p2  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p3  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p4  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p5  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p6  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p7  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p8  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p9  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p10 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p11 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p12 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p13 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		assert( NULL == p13 );
		ins->release( p8 );
		ins->release( p4 );
		ins->release( p2 );
		ins->release( p11 );
		p13 = ins->allocate( ins->kAllocSize );
		assert( NULL == p13 );
		ins->release( p7 );
		ins->release( p5 );
		ins->release( p12 );
		ins->release( p6 );
		ins->release( p9 );
		ins->release( p3 );
		ins->release( p1 );
		ins->release( p10 );
		assert( ins->kAllocSize == ins->freeList_->size );
		assert( ins->kAllocSize == ins->freeList_->next->size );
		assert( ins->kAllocSize == ins->freeList_->next->next->size );
	}

	void MemoryPoolTest::ut6()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		void * p1, * p2, * p3, * p4, * p5, * p6, * p7, * p8, * p9;
		void * p10;
		p1 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p2 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p3 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p4 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p5 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p6 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p7 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p8 = ins->allocate( ins->freeList_->size );
		p9 = ins->allocate( ins->freeList_->size );
		p10 = ins->allocate( 1 );
		assert( NULL == p10 );
		ins->release( p8 );
		ins->release( p4 );
		ins->release( p2 );
		p10 = ins->allocate( ins->kAllocSize );
		assert( NULL != p10 );
		ins->release( p7 );
		ins->release( p5 );
		ins->release( p6 );
		ins->release( p3 );
		ins->release( p9 );
		ins->release( p1 );
		ins->release( p10 );
		assert( ins->kAllocSize==ins->freeList_->size );
		assert( ins->kAllocSize==ins->freeList_->next->size );
		assert( ins->kAllocSize==ins->freeList_->next->next->size );
	}

	void MemoryPoolTest::ut7()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		MemoryPool::Header * header;
		void * p1, * p2, * p3, * p4, * p5, * p6, * p7, * p8, * p9;
		void * p10, * p11, * p12, * p13, * p14, * p15;
		p1  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p2  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p3  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p4  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p5  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p6  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p7  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p8  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p9  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p10 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p11 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p12 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		
		ins->release( p11 );
		ins->release( p10 );
		ins->release( p12 );
		ins->release( p9 );

		for ( header = ins->freeList_; 0 >= header->size; header = header->next );
		p13 = ins->allocate( header->size );

		ins->release( p6 );
		ins->release( p5 );
		ins->release( p8 );
		ins->release( p7 );

		for ( header = ins->freeList_; 0 >= header->size; header = header->next );
		p14 = ins->allocate( header->size );

		ins->release( p3 );
		ins->release( p1 );
		ins->release( p4 );
		ins->release( p2 );

		for ( header = ins->freeList_; 0 >= header->size; header = header->next );
		p15 = ins->allocate( header->size );

		ins->release( p15 );
		ins->release( p13 );
		ins->release( p14 );

		assert( ins->kAllocSize==ins->freeList_->size );
		assert( ins->kAllocSize==ins->freeList_->next->size );
		assert( ins->kAllocSize==ins->freeList_->next->next->size );
	}

	void MemoryPoolTest::ut8()
	{
		MemoryPool * ins = MemoryPool::GetInstance();

		if ( !ins ) { return; }

		void * p1, * p2, * p3, * p4, * p5, * p6, * p7, * p8, * p9;
		void * p10, * p11, * p12, * p13;
		p1  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p2  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p3  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p4  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p5  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p6  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p7  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p8  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p9  = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p10 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p11 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize );
		p12 = ins->allocate( ins->kAllocSize/4 - ins->kHeaderSize - 8 );
		p13 = ins->allocate( 4 );
		ins->release( p8 );
		ins->release( p4 );
		ins->release( p2 );
		ins->release( p11 );
		ins->release( p7 );
		ins->release( p5 );
		ins->release( p12 );
		ins->release( p6 );
		ins->release( p9 );
		ins->release( p3 );
		ins->release( p1 );
		ins->release( p10 );
		ins->release( p13 );

		struct dummy
		{
			MemoryPool::Header header;
			DWORD buffer[4];
		};

		dummy stack;
		ins->release( stack.buffer );
		assert( ins->kAllocSize == ins->freeList_->size );
		assert( ins->kAllocSize == ins->freeList_->next->size );
		assert( ins->kAllocSize == ins->freeList_->next->next->size );
	}
#pragma warning( pop )

} } }
#endif // __TLBOX_MEMORY_TEST__