﻿#pragma once

#include <Windows.h>
#include "misc.h"
#include "atomic.h"

namespace tlbox { namespace singleton
{
	template < typename T >
	class SingletonHelper
		: private misc::Noncopyable
	{
	public:
		template < typename Init >
		static void Initialize( Init init )
		{
			atomic::CallOnlyOnce( initFlag_, init );
		}

		template < typename Init, typename T1 >
		static void Initialize( Init init, T1 a1 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1 );
		}

		template < typename Init, typename T1, typename T2 >
		static void Initialize( Init init, T1 a1, T2 a2 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2 );
		}

		template < typename Init, typename T1, typename T2, typename T3 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5, a6 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5, a6, a7 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5, a6, a7, a8 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9 )
		{
			atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5, a6, a7, a8, a9 );
		}

		template < typename Fin >
		static void Finalize( Fin fin )
		{
			atomic::CallOnlyOnce( finFlag_, fin );
		}

	protected:
		SingletonHelper() {}

	private:
		static atomic::OnceFlag initFlag_;
		static atomic::OnceFlag finFlag_;
	};

	template < typename T >
	atomic::OnceFlag SingletonHelper< T >::initFlag_;

	template < typename T >
	atomic::OnceFlag SingletonHelper< T >::finFlag_;

	template < typename T >
	class Singleton
		: private misc::Noncopyable
	{
	public:
		static void Initialize()
		{
			SingletonHelper< T >::Initialize(
				[]()
				{
					instance_ = new T;
				});
		}

		template < typename T1 >
		static void Initialize( T1 a1 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1 )
				{
					instance_ = new T( a1 );
				},
				a1 );
		}

		template < typename T1, typename T2 >
		static void Initialize( T1 a1, T2 a2 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2 )
				{
					instance_ = new T( a1, a2 );
				},
				a1, a2 );
		}

		template < typename T1, typename T2, typename T3 >
		static void Initialize( T1 a1, T2 a2, T3 a3 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3 )
				{
					instance_ = new T( a1, a2, a3 );
				},
				a1, a2, a3 );
		}

		template < typename T1, typename T2, typename T3, typename T4 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4 )
				{
					instance_ = new T( a1, a2, a3, a4 );
				},
				a1, a2, a3, a4 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5 )
				{
					instance_ = new T( a1, a2, a3, a4, a5 );
				},
				a1, a2, a3, a4, a5 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5, T6 a6 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5, T6 a6 )
				{
					instance_ = new T( a1, a2, a3, a4, a5, a6 );
				},
				a1, a2, a3, a4, a5, a6 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5, T6 a6, T7 a7 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5, T6 a6, T7 a7 )
				{
					instance_ = new T( a1, a2, a3, a4, a5, a6, a7 );
				},
				a1, a2, a3, a4, a5, a6, a7 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5, T6 a6, T7 a7, T8 a8 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8 )
				{
					instance_ = new T( a1, a2, a3, a4, a5, a6, a7, a8 );
				},
				a1, a2, a3, a4, a5, a6, a7, a8 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9 )
				{
					instance_ = new T( a1, a2, a3, a4, a5, a6, a7, a8, a9 );
				},
				a1, a2, a3, a4, a5, a6, a7, a8, a9 );
		}

		static void Finalize()
		{
			SingletonHelper< T >::Finalize(
				[]()
				{
					if ( instance_ )
					{
						delete instance_;
						instance_ = NULL;
					}
				});
		}

		static T * Get()
		{
			return instance_;
		}

	protected:
		Singleton() {}

	private:
		static T * instance_;
	};

	template < typename T >
	T * Singleton< T >::instance_;

} }
