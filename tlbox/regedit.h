﻿#pragma once

#include <tchar.h>
#include <windows.h>
#include <algorithm>
#include <iterator>
#include <string>
#include <utility>
#include <vector>

namespace tlbox { namespace regedit
{
	typedef unsigned __int64			QWORD;
	typedef std::basic_string< TCHAR >	tstring;

	class RegKey;

	// Registry value structure
	struct RegValue
	{
		RegValue() : type( REG_NONE ), multi( 0 ) {}
		
		// binary data
		RegValue(
			LPBYTE data,
			DWORD size )
			: type( REG_BINARY ), multi( data, data + size ) {}

		bool operator==( const RegValue & other ) const
		{
			return type == other.type && multi == other.multi;
		}

		bool operator!=( const RegValue & other ) const
		{
			return !operator==( other );
		}

		bool operator<( const RegValue & other )
		{
			return type < other.type || ( type == other.type && multi < other.multi );
		}

		// 4 byte unsigned int
		RegValue( DWORD data )
			: type( REG_DWORD ), 
			  multi( reinterpret_cast< BYTE * >( &data ), reinterpret_cast< BYTE * >( &data ) + sizeof( DWORD ) ) {}
		
		RegValue & operator=( DWORD data )
		{
			type = REG_DWORD;
			multi.assign( reinterpret_cast< BYTE * >( &data ), reinterpret_cast< BYTE * >( &data ) + sizeof( DWORD ) );

			return *this;
		}
		
		bool operator==( DWORD data ) const
		{
			return type == REG_DWORD && 0 == std::memcmp( &data, &multi[0], sizeof( DWORD ) );
		}

		operator DWORD() const
		{
			if ( type != REG_DWORD ) return 0;

			return *(DWORD *)( &multi[0] );
		}

		// 8 byte unsigned int
		RegValue( QWORD data )
			: type( REG_QWORD ), 
			  multi( reinterpret_cast< BYTE * >( &data ), reinterpret_cast< BYTE * >( &data ) + sizeof( QWORD ) ) {}
		
		RegValue & operator=( QWORD data )
		{
			type = REG_QWORD;
			multi.assign( reinterpret_cast< BYTE * >( &data ), reinterpret_cast< BYTE * >( &data ) + sizeof( QWORD ) );

			return *this;
		}
		bool operator==( QWORD data ) const
		{
			return type == REG_QWORD && 0 == std::memcmp( &data, &multi[0], sizeof( QWORD ) );
		}
		operator QWORD() const
		{
			if ( type != REG_QWORD ) return 0;

			return *(QWORD *)( &multi[0] );
		}

		// null terminated string
		RegValue(
			const TCHAR * data, 
			bool expand = false )
			: type( expand ? REG_EXPAND_SZ : REG_SZ ),
			  multi( (BYTE *)data, (BYTE *)data + sizeof( TCHAR )*( _tcsclen( data ) + 1 ) ) {}
		
		RegValue & operator=( const TCHAR * data )
		{
			type = REG_SZ;
			multi.assign( (BYTE *)data, (BYTE *)data + sizeof( TCHAR ) * ( _tcsclen( data ) + 1 ) );

			return *this;
		}

		bool operator==( const TCHAR * data ) const
		{
			return type == REG_SZ
				&& sizeof( TCHAR ) * ( _tcsclen( data ) + 1 ) == size() 
				&& 0 == std::memcmp( data, &multi[0], size() );
		}

		// only const version
		operator const TCHAR*() const
		{
			return (const TCHAR *)&multi[0];
		}

		// double null terminated array of null terminated strings
		RegValue(
			const TCHAR * * data,
			WORD n ) : type( REG_MULTI_SZ )
		{
			for ( size_t i = 0; i < n; ++ i ) 
			{
				multi.insert( multi.end(), (BYTE *)data[i], (BYTE *)data[i] + sizeof( TCHAR ) * ( _tcsclen( data[i] ) + 1 ) );
			}
			multi.insert( multi.end(), sizeof( TCHAR ), 0 );
		}

		const TCHAR * at( size_t i ) const
		{
			if ( i == 0 && ( type == REG_SZ || type == REG_EXPAND_SZ ) )
			{
				return (const TCHAR *)&multi[0];
			}

			if ( type != REG_MULTI_SZ ) return NULL;

			const TCHAR * b = (const TCHAR *)&multi[0];
			
			while ( i -- )
			{
				b = _tcsninc( b, _tcsclen( b ) );
				
				++ b;
				
				if ( *b == 0 )
				{
					return 0;
				}
			}

			return b;
		}

		// helpers for Data
		DWORD size() const
		{
			return static_cast< DWORD >( multi.size() );
		}

		void resize( DWORD size )
		{
			multi.resize( size );
		}

		LPVOID pointer() const
		{
			return (LPVOID)&multi[0];
		}

		DWORD type;
		std::vector< BYTE > multi;
	};

	// Registry key class
	class RegKey
	{
	public:
		RegKey(
			HKEY key = 0, 
			const TCHAR * machineName = 0 )
			: hive_( key ), key_( key ), sam_( 0 ), path_()
		{
			if ( machineName && *machineName && hive_ ) 
			{
				::RegConnectRegistry( machineName, hive_, &key_ );
			}
		}

		RegKey( const RegKey & other )
			: hive_( other.hive_ ), key_( 0 ), sam_( other.sam_ ), path_( other.path_ )
		{
			open( path_.c_str(), sam_ );
		}

		RegKey & operator=( const RegKey & other )
		{
			if ( this != &other )
			{
				hive_ = other.hive_;
				key_ = 0;
				sam_ = other.sam_;
				path_ = other.path_;
				open( path_.c_str(), sam_ );
			}

			return *this;
		}

		~RegKey()
		{
			if ( key_ && hive_ != key_ )
			{
				::RegCloseKey( key_ );
			}
		}

		// iterate over subkeys
		class iterator 
			: public std::iterator< std::forward_iterator_tag, TCHAR * >
		{
		public:
			value_type operator*()
			{
				return name_.size() ? &name_[0] : 0;
			}

			iterator operator++( int )
			{
				LONG ret;
				DWORD n( max_ );

				iterator old = *this;

				name_.resize( n );
			
				ret = ::RegEnumKeyEx( key_, ++ index_, &name_[0], &n, 0, 0, 0, 0 );
			
				if ( ret != ERROR_SUCCESS ) 
				{
					key_ = 0;
					index_ = 0;
					name_.clear();
				}

				else
				{
					name_.resize( n + 1 );
				}

				return old;
			}

			iterator & operator++()
			{
				LONG ret;
				DWORD n( max_ );

				name_.resize( n );
			
				ret = ::RegEnumKeyEx( key_, ++ index_, &name_[0], &n, 0, 0, 0, 0 );
			
				if ( ret != ERROR_SUCCESS ) 
				{
					key_ = 0;
					index_ = 0;
					name_.clear();
				}

				else
				{
					name_.resize( n + 1 );
				}

				return *this;
			}

			bool operator==( const iterator & other ) const
			{
				return key_ == other.key_ && index_ == other.index_ && name_ == other.name_;
			}

			bool operator!=( const iterator & other ) const
			{
				return !operator==( other );
			}

		private:
			// end of enumeration value
			iterator()
				: key_( 0 ), index_( 0 ), max_( 0 ) {}

			iterator( const HKEY & key )
				: key_( key ), index_( 0 ), max_( 0 )
			{
				DWORD n; // subkeys

				// longest subkey name
				::RegQueryInfoKey( key_, 0, 0, 0, &n, &max_, 0, 0, 0, 0, 0, 0 );
			
				if ( n == 0 )
				{
					key_ = 0;	// end of enumeration

					return;
				}

				name_.resize( ++ max_ );
				n = max_;

				// get
				::RegEnumKeyEx( key_, index_, &name_[0], &n, 0, 0, 0, 0 );
			
				name_.resize( n + 1 );
			}

		private:
			HKEY key_;
			DWORD index_; 
			DWORD max_;
			tstring name_;

			friend class RegKey;
		};

		iterator begin() const
		{
			return iterator( *this );
		}

		iterator end() const
		{
			return iterator();
		}

		// inner class of RegKey
		// for iteraotr over RegValue
		class values
		{
		public:
			// iterator over values
			class iterator 
				: public std::iterator< std::forward_iterator_tag, std::pair< TCHAR *, RegValue > >
			{
			public:
				value_type operator*()
				{
					return name_.size() 
						? std::make_pair( &name_[0], value_ )
						: std::make_pair( static_cast< TCHAR * >( 0 ), value_ );
				}

				iterator * operator->()
				{
					first = ( name_.size() ) ?  &name_[0] : static_cast< TCHAR * >( 0 );
					second = value_;

					return this;
				}

				iterator operator++( int )
				{
					LONG ret;
					DWORD n( nmax_ );
					DWORD v( vmax_ );

					iterator old = *this;

					if ( !key_ )
					{
						return old;
					}
			
					name_.resize( n );
					value_.resize( v );

					ret = ::RegEnumValue( key_, ++ index_, &name_[0], &n, 0, &value_.type, &value_.multi[0], &v );
			
					if ( ret != ERROR_SUCCESS )
					{
						key_ = 0;
						index_ = 0;
						name_.clear();
						value_.multi.clear();
					}
					else
					{
						name_.resize( n + 1 );
						value_.resize( v );
					}

					return old;
				}

				iterator & operator++()
				{
					LONG ret;
					DWORD n( nmax_ );
					DWORD v( vmax_ );

					if ( !key_ )
					{
						return *this;
					}
			
					name_.resize( n );
					value_.resize( v );

					ret = ::RegEnumValue( key_, ++ index_, &name_[0], &n, 0, &value_.type, &value_.multi[0], &v );
			
					if ( ret != ERROR_SUCCESS )
					{
						key_ = 0;
						index_ = 0;
						name_.clear();
						value_.multi.clear();
					}
					else
					{
						name_.resize( n + 1 );
						value_.resize( v );
					}

					return *this;
				}

				bool operator==( const iterator & other ) const
				{
					return key_ == 0 ? other.key_ == 0 : key_ == other.key_ && index_ == other.index_ && name_ == other.name_ && value_ == other.value_;
				}

				bool operator!=( const iterator & other ) const
				{
					return !operator==( other );
				}
		
			private:
				// end of enumeration iterator
				iterator()
					: key_( 0 ), index_( 0 ), nmax_( 0 ), vmax_( 0 ) {}

				iterator( const HKEY & key )
					: key_( key ), index_( 0 ), nmax_( 0 ),  vmax_( 0 )
				{
					DWORD n; // number of values in key
					DWORD v;

					// longest value name and data length
					::RegQueryInfoKey( key_, 0, 0, 0, 0, 0, 0, &n, &nmax_, &vmax_, 0, 0 );
			
					if ( n == 0 ) 
					{
						key_ = 0; // end of enumeration
						return;
					}

					name_.resize( ++ nmax_ );
					value_.resize( vmax_ );

					n = nmax_;
					v = vmax_;
			
					::RegEnumValue( key_, index_, &name_[0], &n, 0, &value_.type, &value_.multi[0], &v );
			
					name_.resize( n + 1 );
					value_.resize( v );
				}

			public:
				value_type::first_type first;		// std::pair< TCHAR *, RegValue >().first
				value_type::second_type second;		// std::pair< TCHAR *, RegValue >().second

			private:
				HKEY key_;
				DWORD index_; 
				DWORD nmax_;		// reg entry name len
				DWORD vmax_;		// reg entry data len
				tstring name_;		// name
				RegValue value_;	// data

				friend values;
			};

			iterator begin() const
			{
				return iterator( key_ );
			}

			iterator end() const
			{
				return iterator();
			}

		private:
			values() {}
			values( const HKEY & key )
				: key_( key ) {}

		private:
			HKEY key_;

			friend class RegKey;
		};

		operator HKEY() const
		{
			return key_;
		}

		HKEY hive() const
		{
			return hive_;
		}

		HKEY key() const
		{
			return hive_ == key_ ? 0 : key_;
		}

		REGSAM sam() const
		{
			return sam_;
		}

		const TCHAR * path() const
		{
			return &path_[0];
		}

		// key has no subkeys
		bool leaf(void) const;

		// key has no values
		bool empty(void) const;

		bool open(
			const TCHAR * subKey, 
			REGSAM sam = KEY_ALL_ACCESS )
		{
			LONG ret;

			if ( key() )
			{
				::RegCloseKey( key() );
			}

			ret = ::RegOpenKeyEx( hive(), subKey, 0, sam, &key_ );

			sam_ = sam;

			path_ = subKey;

			return ERROR_SUCCESS == ret;
		}

		bool create(
			const TCHAR * subKey, 
			REGSAM sam = KEY_ALL_ACCESS )
		{
			LONG ret;
		
			if ( key() )
			{
				::RegCloseKey( key() );
			}

			ret = ::RegCreateKeyEx( hive(), subKey, 0, _T(""), 0, sam, 0, &key_, 0 );
			
			sam_ = sam;
			
			path_ = subKey;

			return ERROR_SUCCESS == ret;
		}

		bool close()
		{
			LONG ret( ERROR_SUCCESS );

			if ( key_ )
			{
				ret = ::RegCloseKey( key() );
				
				key_ = 0;
				
				sam_ = 0;
				
				path_ = _T("");
			}

			return ERROR_SUCCESS == ret;
		}

		// recursive delete
		bool clear();

		values getValues() const
		{
			return values( *this );
		}

		// handle RegValue size issues
		bool queryValue(
			const TCHAR * valueName, 
			RegValue & v ) const
		{
			LONG ret;
			DWORD n;

			if ( v.size() == 0 )
			{
				ret = ::RegQueryValueEx( key(), valueName, 0, &v.type, 0, &n );
				
				if ( ret != ERROR_SUCCESS )
				{
					v = RegValue();
				
					return false;
				}

				v.resize( n );
			}

			n = v.size();

			ret = ::RegQueryValueEx( key_, valueName, 0, &v.type, &v.multi[0], &n );

			if ( ret == ERROR_MORE_DATA || v.size() < n )
			{
				// retry
				v.resize( n );
				ret = ::RegQueryValueEx( key_, valueName, 0, &v.type, &v.multi[0], &n );
			}

			return ERROR_SUCCESS == ret;
		}

		// proxy for RegValue
		class ProxyRegValue 
		{
		public:
			ProxyRegValue(
				RegKey & key, 
				const TCHAR * path )
				: key_( key ), path_( path ), result_( true ) {}

			ProxyRegValue & operator=( const ProxyRegValue & other )
			{
				if ( this != &other ) 
				{
					key_ = other.key_;
					path_ = other.path_;
				}

				result_ = true;

				return *this;
			}

			ProxyRegValue & operator=( const RegValue & other )
			{
				result_ = key_.setValue( path_, other );

				return *this;
			}

			operator bool() const
			{
				return result_;
			}

			bool operator==( const RegValue & other ) const
			{
				return operator RegValue().operator==( other );
			}

			bool operator==( const ProxyRegValue & other ) const
			{
				return operator RegValue().operator==( other.queryValue() );
			}

			RegValue queryValue() const
			{
				RegValue v;

				key_.queryValue( path_, v );

				return v;
			}

			// for r-value
			operator RegValue() const
			{
				return queryValue();
			}

		private:
			RegKey &  key_;
			const TCHAR * path_;
			bool result_;
		};

		// use proxy for get-set value operations
		ProxyRegValue operator[]( const TCHAR * valueName )
		{
			return ProxyRegValue( *this, valueName );
		}

		bool setValue(
			const TCHAR * valueName, 
			const RegValue & v )
		{
			return ERROR_SUCCESS == ::RegSetValueEx( key_, valueName, 0, v.type, &v.multi[0], v.size() );
		}

		bool enumKey(
			DWORD index, 
			LPTSTR name, 
			LPDWORD nameCount )
		{
			return ERROR_SUCCESS == ::RegEnumKeyEx( key_, index, name, nameCount, 0, 0, 0, 0 );
		}

		bool enumValue(
			DWORD index, 
			LPTSTR valueName, 
			LPDWORD valueNameCount, 
			RegValue & v )
		{
			DWORD n( v.size() );

			if ( n == 0 )
			{
				::RegEnumValue( key_, index, valueName, valueNameCount, 0, &v.type, 0, &n );
				v.resize( n );
			}

			return ERROR_SUCCESS == ::RegEnumValue( key_, index, valueName, valueNameCount, 0, &v.type, &v.multi[0], &n );
		}

	private:
		HKEY hive_; 
		HKEY key_;
		REGSAM sam_;
		tstring path_;
	};

	// key has no subkeys
	inline bool RegKey::leaf() const
	{
		return begin() == end();
	}

	// key has no values
	inline bool RegKey::empty() const
	{
		values v( *this );

		return v.begin() == v.end();
	}

	// recursively delete key and all subkeys
	inline bool RegKey::clear()
	{
		if ( !hive() || !path() )
		{
			return false;
		}

		// have to copy all subkeys
		std::vector< tstring > subkey;
		
		std::copy( begin(), end(), std::back_inserter( subkey ) );

		for ( size_t i = 0; i < subkey.size(); ++ i ) 
		{
			RegKey k( hive() );
			k.open( ( tstring( path() ) + _T("\\") + subkey[i] ).c_str() );
			k.clear();
		}

		return ( ( ERROR_SUCCESS == ::RegDeleteKey( hive(), path() ) ) ? true : false );
	}
	
} }