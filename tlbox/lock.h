﻿#pragma once

#include <Windows.h>
#include <assert.h>
#include <unordered_set>
#include <type_traits>
#include <exception>
#include "misc.h"
#include "atomic.h"

//#ifdef __TLBOX_SAFELOCK__
//#pragma message ( " ---- compile tlbox::lock (safe) ----" )
//#else
//#pragma message ( " ---- compile tlbox::lock ----" )
//#endif

namespace tlbox { namespace lock { namespace
{
	class LockOwner
	{
	public:
		explicit LockOwner( uint32_t id ) : id_( id ), count_( 1 ) {}
		~LockOwner() {}

		struct hash
			: std::unary_function< LockOwner, size_t >
		{
			size_t operator()( const LockOwner & owner ) const
			{
				return std::hash< size_t >()( owner.id_ );
			}
		};

		struct eq
			: std::binary_function< LockOwner, LockOwner, bool >
		{
			bool operator()(
				const LockOwner & a, 
				const LockOwner & b ) const
			{
				return a == b;
			}
		};

		bool operator==( const LockOwner & other ) const
		{
			return this->id_ == other.id_;
		}

		LockOwner operator++( int ) const
		{
			LockOwner old = *this; 
			count_ ++; 
			return old; 
		}
		
		LockOwner operator--( int ) const
		{
			LockOwner old = *this;
			if ( count_ > 0 )
			{
				count_ --; 
			}
			return old; 
		}

		bool alive() const
		{
			bool al = false;
			HANDLE th = ::OpenThread( SYNCHRONIZE, FALSE, id_ );
			if ( NULL != th )
			{
				al = ( WAIT_OBJECT_0 == ::WaitForSingleObject( th, 0 ) ) ? false : true;
				::CloseHandle( th );
			}
			return al;
		}

		uint32_t id() const
		{
			return id_;
		}

		uint32_t count() const
		{
			return count_;
		}

	private:
		uint32_t id_;
		mutable uint32_t count_;
	};

	class LockOwnerGroup
	{
	public:
		typedef std::unordered_set< LockOwner, LockOwner::hash, LockOwner::eq >		Owners;

	public:
		LockOwnerGroup() : owners_() {}
		~LockOwnerGroup() {}

		void add( uint32_t id )
		{
			Owners::const_iterator it = get( id );
			if ( end() != it )
			{
				(*it) ++;
			}
			else
			{
				owners_.insert( LockOwner( id ) );
			}
		}

		Owners::const_iterator remove( uint32_t id )
		{
			Owners::const_iterator it = get( id );
			if ( end() != it )
			{
				(*it) --;
				if ( 0 == it->count() )
				{
					it = erase( it );
				}
			}
			return it;
		}

		Owners::const_iterator remove( Owners::const_iterator it )
		{
			if ( end() != it )
			{
				(*it) --;
				if ( 0 == it->count() )
				{
					it = erase( it );
				}
			}
			return it;
		}

		Owners::const_iterator begin() const
		{
			return owners_.begin();
		}

		Owners::const_iterator end() const
		{
			return owners_.end();
		}

		Owners::const_iterator get( uint32_t id ) const
		{
			return owners_.find( LockOwner( id ) );
		}

		Owners::const_iterator erase( Owners::const_iterator it )
		{
			owners_.erase( it ++ );

			return it;
		}

		Owners::size_type size() const
		{
			return owners_.size();
		}

	private:
		Owners owners_;
	};

} } }

namespace tlbox { namespace lock
{
	template < class T >
	class Exclusion
		: private misc::Noncopyable
	{
	public:
		Exclusion() {}
		virtual ~Exclusion() {}

		typedef T param_type;

	public:
		virtual bool trylock( const T & param ) = 0;
		virtual void lock( const T & param ) = 0;
		virtual void unlock( const T & param ) = 0;
	};

	template <>
	class Exclusion< void >
		: private misc::Noncopyable
	{
	public:
		Exclusion() {}
		virtual ~Exclusion() {}

		typedef void param_type;

	public:
		virtual bool trylock() = 0;
		virtual void lock() = 0;
		virtual void unlock() = 0;
	};

	template < class T >
	class ScopedLock
	{
	private:
		template < class U >
		class Impl
		{
		public:
			Impl(
			__in Exclusion< U > & exobj,
			__in const U & param )
				: exobj_( exobj )
				, param_( param )
			{
				exobj_.lock( param_ );
			}

			~Impl()
			{
				exobj_.unlock( param_ );
			}

		private:
			Exclusion< U > & exobj_;
			const U param_;
		};

		template <>
		class Impl< void >
		{
		public:
			Impl( __in Exclusion< void > & exobj )
				: exobj_( exobj )
			{
				exobj_.lock();
			}

			~Impl()
			{
				exobj_.unlock();
			}

		private:
			Exclusion< void > & exobj_;
		};

	public:
		// SFINAE
		template < class V >
		ScopedLock(
			__in V & exobj,
			__in const typename std::enable_if< !std::is_void< typename V::param_type >::value, typename V::param_type >::type & param )
				: impl_( exobj, param ) {}
		
		// SFINAE
		template < class V >
		explicit ScopedLock( __in V & exobj )
				: impl_( exobj ) {}

		~ScopedLock() {}

	private:
		Impl< typename T::param_type > impl_;
	};

	template < class T >
	class ManualLock
	{
	private:
		template < class U >
		class Impl
		{
		public:
			Impl(
			__in Exclusion< U > & exobj,
			__in const U & param )
				: exobj_( exobj )
				, param_( param ) {}

			~Impl() {}

			bool tryAcquire()
			{
				return exobj_.trylock( param_ );
			}

			void acquire()
			{
				exobj_.lock( param_ );
			}

			void release()
			{
				exobj_.unlock( param_ );
			}

		private:
			Exclusion< U > & exobj_;
			const U param_;
		};

		template <>
		class Impl< void >
		{
		public:
			Impl( __in Exclusion< void > & exobj )
				: exobj_( exobj ) {}

			~Impl() {}

			bool tryAcquire()
			{
				return exobj_.trylock();
			}

			void acquire()
			{
				exobj_.lock();
			}

			void release()
			{
				exobj_.unlock();
			}

		private:
			Exclusion< void > & exobj_;
		};

	public:
		// SFINAE
		template < class V >
		ManualLock(
			__in V & exobj,
			__in const typename std::enable_if< !std::is_void< typename V::param_type >::value, typename V::param_type >::type & param )
				: impl_( exobj, param ) {}
		
		// SFINAE
		template < class V >
		explicit ManualLock( __in V & exobj )
				: impl_( exobj ) {}

		~ManualLock() {}

		bool tryAcquire()
		{
			return impl_.tryAcquire();
		}

		void acquire()
		{
			impl_.acquire();
		}

		void release()
		{
			impl_.release();
		}

	private:
		Impl< typename T::param_type > impl_;
	};

	class CriticalSection
	{
	public:
		CriticalSection() 
		{
#ifdef __TLBOX_SAFELOCK__
			if ( NULL == ( hmutex_ = ::CreateMutex( NULL, FALSE, NULL ) ) )
			{
				throw std::exception();
			}
#else
			::InitializeCriticalSection( &csobj_ );
#endif
		}

		~CriticalSection()
		{
#ifdef __TLBOX_SAFELOCK__
			if ( NULL != hmutex_ )
			{
				::CloseHandle( hmutex_ );
			}
#else
			::DeleteCriticalSection( &csobj_ );
#endif
		}

		bool tryenter()
		{
#ifdef __TLBOX_SAFELOCK__
			DWORD wresult = ::WaitForSingleObject( hmutex_, 0 );
			return ( WAIT_OBJECT_0 == wresult || WAIT_ABANDONED == wresult );
#else
			return ::TryEnterCriticalSection( &csobj_ ) ? true : false;
#endif
		}

		void enter()
		{
#ifdef __TLBOX_SAFELOCK__
			::WaitForSingleObject( hmutex_, INFINITE );
#else
			::EnterCriticalSection( &csobj_ );
#endif
		}

		void leave()
		{
#ifdef __TLBOX_SAFELOCK__
			::ReleaseMutex( hmutex_ );
#else
			::LeaveCriticalSection( &csobj_ );
#endif
		}

	private:
#ifdef __TLBOX_SAFELOCK__
			HANDLE hmutex_;
#else
			CRITICAL_SECTION csobj_;
#endif
	};

	class Mutex
		: public Exclusion< void >
	{
	public:
		typedef Exclusion< void >::param_type param_type;

	public:
		Mutex() : cs_() {}
		~Mutex() {}

	private:
		bool trylock()
		{
			return cs_.tryenter();
		}

		void lock()
		{
			cs_.enter();
		}

		void unlock() 
		{
			cs_.leave();
		}

	private:
		CriticalSection cs_;
	};

	class SpinMutex
		: public Exclusion< void >
	{
	public:
		SpinMutex() {}
		~SpinMutex() {}

	private:
		bool trylock()
		{
			if ( ::GetCurrentThreadId() == impl_.currentval )
			{
				return true;
			}

			if ( impl_.initval == atomic::CompareAndExchange(
				impl_.currentval, 
				impl_.initval, 
				impl_.newval ) )
			{
				impl_.currentval = ::GetCurrentThreadId();
				return true;
			}

			return false;
		}

		void lock()
		{
			for ( ; ; )
			{
				if ( ::GetCurrentThreadId() == impl_.currentval )
				{
					break;
				}

				if ( impl_.initval == atomic::CompareAndExchange(
					impl_.currentval, 
					impl_.initval, 
					impl_.newval ) )
				{
					impl_.currentval = ::GetCurrentThreadId();
					break;
				}

				for ( ; impl_.currentval != impl_.initval; )
				{
					::SwitchToThread();
				}
			}
		}

		void unlock() 
		{
			if ( ::GetCurrentThreadId() == impl_.currentval )
			{
				atomic::CompareAndExchange(
					impl_.currentval, 
					::GetCurrentThreadId(),
					impl_.initval );
			}
		}

	private:
		struct Impl
		{
			Impl() : initval( 0 ), currentval( initval ), newval( 0xCAFEBABE ) {}

			const long initval;
			volatile long currentval;
			long newval;
		}
		impl_;
	};

	class RWType
	{
	private:
		explicit RWType( long value )
			: value_( value ) {}

	public:
		operator long() const
		{
			return value_;
		}

	public:
		static const long kInit = 0;
		static const long kReader = 1;
		static const long kWriter = 2;

	private:
		long value_;

		friend class Reader;
		friend class Writer;
	};

	class Reader
		: public RWType
	{
	public:
		Reader() 
			: RWType( RWType::kReader ) {}
	};

	class Writer
		: public RWType
	{
	public:
		Writer() 
			: RWType( RWType::kWriter ) {}
	};

	class RWMutex
		: public Exclusion< RWType >
	{
	public:
		typedef Exclusion< RWType >::param_type param_type;

	public:
#ifdef __TLBOX_SAFELOCK__
		explicit RWMutex( uint32_t cycle = 1000 )
			: owners_()
			, wcs_()
			, rcs_()
			, cycle_( cycle )
#else
		RWMutex()
			: wcs_()
			, rcs_()
			, cycle_( INFINITE )
#endif
		{
			hevent_ = ::CreateEvent( NULL, TRUE, TRUE, NULL );

			if ( NULL == hevent_ )
			{
				throw std::exception();
			}

			rcount_ = 0;
		}

		~RWMutex()
		{
			if ( NULL != hevent_ )
			{
				::CloseHandle( hevent_ );
			}
		}

	private:
		bool trylock( const RWType & param )
		{
			switch ( param )
			{
			case RWType::kReader:
				{
					bool enterable = false;
					rcs_.enter();
					enterable = wcs_.tryenter();
					if ( enterable )
					{
						getrlock();
						wcs_.leave();
					}
					rcs_.leave();
					return enterable;
				}
				break;
			case RWType::kWriter:
#ifdef __TLBOX_SAFELOCK__
				rcs_.enter();
				adjrlock();
				rcs_.leave();
#endif
				return ( ( wcs_.tryenter() && ( ::WaitForSingleObject( hevent_, 0 ) == WAIT_OBJECT_0 ) ) ? true : false );
				break;
			};

			return false;
		}

		void lock( const RWType & param )
		{
			wcs_.enter();
			switch ( param )
			{
			case RWType::kReader:
				rcs_.enter();
				getrlock();
				rcs_.leave();
				wcs_.leave();
				break;
			case RWType::kWriter:
#ifdef __TLBOX_SAFELOCK__
				do
				{
					rcs_.enter();
					adjrlock();
					rcs_.leave();
				}
				while ( WAIT_TIMEOUT == ::WaitForSingleObject( hevent_, cycle_ ) );
#else
				::WaitForSingleObject( hevent_, INFINITE );
#endif
				break;
			};
		}

		void unlock( const RWType & param ) 
		{
			switch ( param )
			{
			case RWType::kReader:
				rcs_.enter();
				putrlock();
				rcs_.leave();
				break;
			case RWType::kWriter:
				wcs_.leave();
				break;
			};
		}

		void getrlock()
		{
#ifdef __TLBOX_SAFELOCK__
			owners_.add( ::GetCurrentThreadId() );
#endif
			if ( ++ rcount_ == 1 )
			{
				::ResetEvent( hevent_ );
			}
		}

		void putrlock()
		{
			if ( 0 < rcount_ && ( -- rcount_ == 0 ) )
			{
				::SetEvent( hevent_ );
			}
#ifdef __TLBOX_SAFELOCK__
			owners_.remove( ::GetCurrentThreadId() );
#endif
		}

#ifdef __TLBOX_SAFELOCK__
		void adjrlock()
		{
			for ( auto it = owners_.begin(); it != owners_.end(); )
			{
				if ( !it->alive() )
				{
					rcount_ -= std::min< uint32_t >( rcount_, it->count() );
					if ( 0 == rcount_ )
					{
						::SetEvent( hevent_ );
					}
					it = owners_.erase( it );
				}
				else
				{
					++ it;
				}
			}
		}
#endif

	private:
#ifdef __TLBOX_SAFELOCK__
		LockOwnerGroup owners_;
#endif
		CriticalSection wcs_;
		CriticalSection rcs_;
		uint32_t cycle_;
		HANDLE hevent_;
		long rcount_;
	};

} }
