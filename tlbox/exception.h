﻿#pragma once

#include <Windows.h>
#include <exception>
#include <string>
#include <sstream>
#include <iomanip>

namespace tlbox { namespace exception
{
	class SEHException
		: public std::exception
	{
	public:
		SEHException( const EXCEPTION_POINTERS & ep )
			: exceptionPointers_( ep ), what_()
		{
			unsigned int code = ep.ExceptionRecord->ExceptionCode;
			void * address = ep.ExceptionRecord->ExceptionAddress;
			std::string message = GetExceptionMessage( code );

			std::ostringstream oss;
			oss << "Exception occured !!!" << std::endl
				<< message << std::endl
				<< "Exception code:" << std::endl
				<< "    0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << code << std::endl
				<< "Exception address:" << std::endl
				<< "    0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << reinterpret_cast< unsigned int >( address ) << std::endl
				<< GetRegisters( ep );

			what_ = oss.str();
		}

		virtual ~SEHException() {}

		virtual const char * what() const
		{
			return what_.c_str();
		}
	
		unsigned int getExceptionCode() const
		{
			return exceptionPointers_.ExceptionRecord->ExceptionCode;
		}

		const EXCEPTION_POINTERS * getExceptionPoniters() const
		{
			return &exceptionPointers_;
		}

		static std::string GetExceptionMessage( unsigned int ec )
		{
			std::string message;
			char * buf = NULL;

			HMODULE mod = ::GetModuleHandle( L"ntdll.dll" );

			if ( mod )
			{
				::FormatMessageA(
					FORMAT_MESSAGE_ALLOCATE_BUFFER |
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_FROM_HMODULE,
					mod,
					ec,
					MAKELANGID( LANG_ENGLISH, SUBLANG_DEFAULT ),
					reinterpret_cast< char * >( &buf ),
					0,
					NULL );

				if ( buf )
				{
					message = buf;
					::LocalFree( buf );
				}
			}

			return message;
		}

		std::string GetRegisters( const EXCEPTION_POINTERS & ep )
		{
			PCONTEXT ctx = ep.ContextRecord;

			std::ostringstream oss;
			oss << std::setw( 8 ) << std::hex << std::setfill( '0' )
				<< "Registers: " << std::endl
				<< "    eax: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Eax << std::endl
				<< "    ebx: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Ebx << std::endl
				<< "    ecx: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Ecx << std::endl
				<< "    edx: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Edx << std::endl
				<< "    edi: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Edi << std::endl
				<< "    esi: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Esi << std::endl
				<< "    eip: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Eip << std::endl
				<< "    ebp: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Ebp << std::endl
				<< "    esp: 0x" << std::setw( 8 ) << std::hex << std::setfill( '0' ) << ctx->Esp;

			return oss.str();
		}

	private:
		SEHException();

	private:
		EXCEPTION_POINTERS exceptionPointers_;
		std::string what_;
	};

	class SETranslator
	{
	public:
		SETranslator()
			: prevTranslator_( ::_set_se_translator( Translate ) ) {}
	
		virtual ~SETranslator()
		{
			::_set_se_translator( prevTranslator_ );
		}

	private:
		static void Translate(
			unsigned int ec,
			EXCEPTION_POINTERS * ep )
		{
			UNREFERENCED_PARAMETER( ec );

			throw SEHException( *ep );
		}

	private:
		_se_translator_function prevTranslator_;
	};

} }