﻿#pragma once

#define __TLBOX_MEMORY_TEST__		(0)

#include <stdint.h>
#include <memory>
#if __TLBOX_MEMORY_TEST__
#include "memory_test.h"
#endif

namespace tlbox { namespace memory
{
	bool Initialize(
		__in size_t allocSize, 
		__in size_t initialAllocSize = 0,
		__in uint32_t maxAllocCount = 0 );
	
	void Finalize();

	void * AllocatePool( __in size_t size );
	
	void ReleasePool( __in void * buffer );

	// PoolAllocator template class
	//
	// 注意点
	//   このアロケータは一回に確保するヒープサイズが決まっているので、
	//   要素を追加する度に連続した領域サイズが大きくなる vector はなるべく使用しない
	//   vector に要素を追加していき、全体の連続した領域サイズがあるタイミングでアロケ
	//   ータが一度に確保できるサイズを上回ると結果として std::bad_alloc 例外が発生する
	template < class T >
	class PoolAllocator
		: public std::allocator< T >
	{
	public:
		PoolAllocator() throw() {}
		PoolAllocator( const PoolAllocator & ) throw() {}
		template < class U > PoolAllocator( const PoolAllocator< U > & ) throw() {}

		pointer allocate( size_type num, const_pointer hint = 0 )
		{
			pointer ptr = ( pointer )( AllocatePool( num * sizeof( T ) ) );

			if ( 0 == ptr )
			{
				throw std::bad_alloc();
			}
		
			return ptr;
		}

		void deallocate( pointer p, size_type num )
		{
			ReleasePool( p );
		}

		template < class U >
		struct rebind
		{
			typedef PoolAllocator< U > other;
		};
	};

} } // namespace heap
