﻿#pragma once

#include "misc.h"
#include <utility>
#include <type_traits>
#include <exception>

namespace tlbox { namespace container
{
	template <
		size_t Capacity,
		typename KeyTy, 
		typename ValueTy, 
		typename HashFn = misc::HashFunctor< KeyTy >, 
		typename EqFn = misc::EqFunctor< KeyTy > >
	class FixedHashTable
	{
	public:
		FixedHashTable();
		~FixedHashTable() {};

		struct Pair
		{
			Pair() : first(), second() {}

			// move ctor
			Pair( Pair && other )
			{
				first = other.first;
				second = std::move( other.second );
			}

			// move assignment operator
			Pair & operator=( Pair && other )
			{
				first = other.first;
				second = std::move( other.second );
				return *this;
			}

			KeyTy first;
			ValueTy second;
		};

		class Element
		{
		public:
			Element() : active( false ) {}
			
			// move ctor
			Element( Element && other )
			{
				pair = std::move( other.pair );
				active = other.active;
			}

			// move assignment operator
			Element & operator=( Element && other )
			{
				pair = std::move( other.pair );
				active = other.active;
				return *this;
			}

		private:
			Pair pair;
			bool active;

			friend class FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >;
		};

		class iterator
		{
		public:
			iterator operator++( int )
			{
				iterator old = *this;

				element_ = hashTable_->next( element_ );

				return old;
			}

			iterator & operator++()
			{
				element_ = hashTable_->next( element_ );

				return *this;
			}

			Pair & operator*()
			{
				return element_->pair;
			}

			Pair * operator->()
			{
				return &element_->pair;
			}

			bool operator==( const iterator & other )
			{
				return element_ == other.element_;
			}

			bool operator!=( const iterator & other )
			{
				return element_ != other.element_;
			}

		private:
			iterator(
				const FixedHashTable * hashTable,
				Element * element )
					: hashTable_( hashTable ), element_( element ) {}

		private:
			const FixedHashTable * hashTable_;
			Element * element_;

			friend class FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >;
		};

		iterator begin() const
		{
			return iterator( this, this->head() );
		}

		iterator end() const
		{
			return iterator( this, NULL );
		}

		iterator find( const KeyTy & key )
		{
			return iterator( this, this->lookup( key ) );
		}

		void erase( const iterator & it )
		{
			remove( it.element_ );
		}

		size_t size() const
		{
			return occupancy_;
		}

		size_t capacity() const
		{
			return capacity_;
		}

		bool empty() const
		{
			return 0 == occupancy_;
		}

		bool full() const
		{
			return capacity_ == occupancy_;
		}

		ValueTy & operator[]( const KeyTy & key )
		{
			iterator it = find( key );

			if ( this->end() == it )
			{
				if ( this->end() == ( it = insert( key, ValueTy() ) ) )
				{
					throw std::length_error( "FixedHashTable called operator[]( const KeyTy & key )" );
				}
			}

			return it->second;
		}

		iterator insert(
			const KeyTy & key,
			const ValueTy & value );

		iterator insert(
			const KeyTy & key,
			ValueTy && value );
#if 0
		// SFINAE
		template < typename OverrideValueTy >
		iterator insert(
			const KeyTy & key,
			OverrideValueTy && value,
			typename std::enable_if< std::is_convertible<
				typename std::remove_reference< OverrideValueTy >::type *, 
				typename std::remove_reference< ValueTy >::type * >::value >::type * = NULL );
#endif
		void remove( const KeyTy & key );

		void clear();

	private:
		Element * lookup( const KeyTy & key );

		void remove( Element * element );

		Element * head() const;

		Element * tail() const
		{
			return const_cast< Element * >( table_ ) + capacity_; 
		}

		Element * next( Element * element ) const;

		void forward( Element * & element ) const
		{
			if ( ++ element >= tail() )
			{
				element = const_cast< Element * >( table_ );
			}
		}

		Element * get( const KeyTy & key );

	private:
		template < size_t N >
		struct Aligned
		{
			enum { width = sizeof( size_t ) };
			enum { size = ( N + ( width - 1 ) ) & ~( width - 1 ) };
			static_assert( 0 < size, "illegal template-parameter in \"Capacity\".");
		};

		size_t capacity_;
		size_t occupancy_;

		Element table_[Aligned< Capacity >::size];
	};

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::FixedHashTable()
		: capacity_( Aligned< Capacity >::size )
		, occupancy_( 0 )
	{
		clear();
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::Element *
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::lookup( const KeyTy & key )
	{
		Element * e = get( key );

		if ( e != NULL && e->active )
		{
			return e;
		}

		return NULL;
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::iterator
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::insert(
		const KeyTy & key,
		const ValueTy & value )
	{
		Element * e = get( key );

		if ( e != NULL )
		{
			if ( e->active )
			{
				e->pair.second = value;		// overwrite
			}
			else if ( occupancy_ < capacity_ )
			{
				e->pair.first = key;
				e->pair.second = value;
				e->active = true;

				occupancy_ ++;
			}
		}

		return iterator( this, e );
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::iterator
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::insert(
		const KeyTy & key,
		ValueTy && value )
	{
		Element * e = get( key );

		if ( e != NULL )
		{
			if ( e->active )
			{
				e->pair.second = std::forward< ValueTy >( value );		// overwrite (perfect-forwarding)
			}
			else if ( occupancy_ < capacity_ )
			{
				e->pair.first = key;
				e->pair.second = std::forward< ValueTy >( value );		// perfect-forwarding
				e->active = true;

				occupancy_ ++;
			}
		}

		return iterator( this, e );
	}
#if 0
	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	template < typename OverrideValueTy >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::iterator
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::insert(
		const KeyTy & key,
		OverrideValueTy && value,
		typename std::enable_if< std::is_convertible<
			typename std::remove_reference< OverrideValueTy >::type *, 
			typename std::remove_reference< ValueTy >::type * >::value >::type * = NULL )
	{
		Element * e = get( key );

		if ( e != NULL )
		{
			if ( e->active )
			{
				e->pair.second = static_cast< OverrideValueTy && >( value );		// overwrite (perfect-forwarding)
			}
			else if ( occupancy_ < capacity_ )
			{
				e->pair.first = key;
				e->pair.second = static_cast< OverrideValueTy && >( value );		// perfect-forwarding
				e->active = true;

				occupancy_ ++;
			}
		}

		return iterator( this, e );
	}
#endif
	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	void FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::remove( Element * e )
	{
		Element * start = e;
		Element * next = e;

		do
		{
			// forward
			forward( next );

			if ( !next->active )
			{
				break;
			}

			Element * optimum = table_ + ( HashFn()( next->pair.first ) % capacity_ );

			// relocation
			if ( ( next > e && ( optimum <= e || optimum > next ) )
				|| ( next < e && ( optimum <= e && optimum > next ) ) )
			{
				// move
				*e = std::move( *next );
				e = next;
			}
		}
		while ( next != start );

		e->active = false;

		occupancy_ --;
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	void FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::remove( const KeyTy & key )
	{
		Element * e = get( key );

		if ( e != NULL && e->active )
		{
			remove( e );
		}
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	void FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::clear()
	{
		const Element * ter = tail();

		for ( Element * e = table_; e < ter; e ++ )
		{
			e->active = false;
		}
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::Element *
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::head() const
	{
		return next( const_cast< Element * >( table_ ) - 1 );
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::Element *
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::next( Element * e ) const
	{
		const Element * ter = tail();

		for ( e ++; e < ter; e ++ )
		{
			if ( e->active )
			{
				return e;
			}
		}

		return NULL;
	}

	template < size_t Capacity, typename KeyTy, typename ValueTy, typename HashFn, typename EqFn >
	typename FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::Element *
	FixedHashTable< Capacity, KeyTy, ValueTy, HashFn, EqFn >::get( const KeyTy & key )
	{
		Element * e = table_ + ( HashFn()( key ) % capacity_ );

		const Element * start = e;
		const Element * ter = tail();

		while ( e->active 
			&& ( HashFn()( key ) != HashFn()( e->pair.first )
				|| !EqFn()( key, e->pair.first ) ) )
		{
			// forward
			forward( e );

			if ( e == start )
			{
				return NULL;		// detect turned around
			}
		}

		return e;
	}

} }