﻿#pragma once

#include <string>
#include <vector>

namespace tlbox { namespace base64 { namespace { namespace impl
{
	inline bool Encode(
		const std::vector< unsigned char > & src,
		std::string & dst )
	{
		const std::string ctable = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		std::string result;

		for ( size_t i = 0; i < src.size(); i ++ )
		{
			switch ( i % 3 )
			{
			case 0:
				result.push_back( ctable[( src[i] & 0xFC ) >> 2] );		// convert 6 bit of the first
				if ( src.size() == i + 1 )								// next is last byte
				{
					result.push_back( ctable[( src[i] & 0x03 ) << 4] );
					result.push_back( '=' );
					result.push_back( '=' );
				}
				break;
			case 1:
				result.push_back( ctable[( ( src[i - 1] & 0x03 ) << 4 ) | ( ( src[i] & 0xF0 ) >> 4 )] );
				if ( src.size() == i + 1 )
				{
					result.push_back( ctable[( src[i] & 0x0F ) << 2] );
					result.push_back( '=' );
				}
				break;
			case 2:
				result.push_back( ctable[( ( src[i - 1] & 0x0F ) << 2 ) | ( ( src[i] & 0xC0 ) >> 6 )] );
				result.push_back( ctable[src[i] & 0x3F] );
				break;
			}
		}

		dst.swap( result );

		return true;
	}

	inline bool Decode(
		const std::string & src,
		std::vector< unsigned char > & dst )
	{
		if ( src.size() & 0x00000003 )
		{
			return false;
		}
		
		const std::string ctable = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		std::vector< unsigned char > result;

		for ( size_t i = 0; i < src.size(); i += 4 )
		{
			if ( '=' == src[i] || '=' == src[i + 1] )
			{
				return false;
			}

			if ( '=' == src[i + 2] )
			{
				const std::string::size_type s0 = ctable.find( src[i] );
				const std::string::size_type s1 = ctable.find( src[i + 1] );

				if ( std::string::npos == s0 || std::string::npos == s1 )
				{
					return false;
				}

				result.push_back( static_cast< unsigned char >( ( ( s0 & 0x3F ) << 2 ) | ( ( s1 & 0x30 ) >> 4 ) ) );
				
				break;
			}
			else if ( '=' == src[i + 3] )
			{
				const std::string::size_type s0 = ctable.find( src[i] );
				const std::string::size_type s1 = ctable.find( src[i + 1] );
				const std::string::size_type s2 = ctable.find( src[i + 2] );

				if ( std::string::npos == s0 || std::string::npos == s1 || std::string::npos == s2 )
				{
					return false;
				}

				result.push_back( static_cast< unsigned char >( ( ( s0 & 0x3F ) << 2 ) | ( ( s1 & 0x30 ) >> 4 ) ) );
				result.push_back( static_cast< unsigned char >( ( ( s1 & 0x0F ) << 4 ) | ( ( s2 & 0x3C ) >> 2 ) ) );

				break;
			}
			else
			{
				const std::string::size_type s0 = ctable.find( src[i] );
				const std::string::size_type s1 = ctable.find( src[i + 1] );
				const std::string::size_type s2 = ctable.find( src[i + 2] );
				const std::string::size_type s3 = ctable.find( src[i + 3] );

				if ( std::string::npos == s0 || std::string::npos == s1 || std::string::npos == s2 || std::string::npos == s3 )
				{
					return false;
				}

				result.push_back( static_cast< unsigned char >( ( ( s0 & 0x3F ) << 2 ) | ( ( s1 & 0x30 ) >> 4 ) ) );
				result.push_back( static_cast< unsigned char >( ( ( s1 & 0x0F ) << 4 ) | ( ( s2 & 0x3C ) >> 2 ) ) );
				result.push_back( static_cast< unsigned char >( ( ( s2 & 0x03 ) << 6 ) | ( s3 & 0x3F ) ) );
			}
		}

		dst.swap( result );

		return true;
	}

} } } }

namespace tlbox { namespace base64
{
	namespace multi
	{
		inline std::string Encode( const std::string & src )
		{
			std::string enc;
			std::vector< unsigned char > temp;

			temp.resize( src.size() );
			std::copy( src.begin(), src.end(), temp.begin() );

			if ( !impl::Encode( temp, enc ) )
			{
				enc.clear();
			}

			return enc;
		}

		inline std::string Encode(
			const char * src,
			size_t size )
		{
			std::string s( src, size );

			return Encode( s );
		}

		inline std::string Decode( const std::string & src )
		{
			std::string dec;
			std::vector< unsigned char > temp;

			if ( !impl::Decode( src, temp ) )
			{
				dec.clear();
			
				return dec;
			}

			dec.resize( temp.size() );

			std::copy( temp.begin(), temp.end(), dec.begin() );

			return dec;
		}
	}

	namespace wide
	{
		inline std::string Encode( const std::wstring & wsrc )
		{
			using namespace base64;

			size_t size = wsrc.size() * sizeof( std::wstring::value_type );
			const unsigned char * p = reinterpret_cast< const unsigned char * >( wsrc.data() );
			std::string enc;
			std::vector< unsigned char > temp;

			temp.resize( size );

			for ( size_t i = 0; i < size; i += sizeof( std::wstring::value_type ) )
			{
				temp[i] = p[i + 1];
				temp[i + 1] = p[i];
			}

			if ( !impl::Encode( temp, enc ) )
			{
				enc.clear();
			}

			return enc;
		}

		inline std::string Encode(
			const wchar_t * wsrc,
			size_t size )
		{
			std::wstring ws( wsrc, size );

			return Encode( ws );
		}

		inline std::wstring Decode( const std::string & src )
		{
			std::wstring wdec;
			std::vector< unsigned char > temp;

			if ( !impl::Decode( src, temp ) )
			{
				wdec.clear();

				return wdec;
			}

			for ( size_t i = 0; i < temp.size(); i += sizeof( std::wstring::value_type ) )
			{
				std::swap( temp[i], temp[i + 1] );
			}

			size_t size = temp.size() / sizeof( std::wstring::value_type );
			const wchar_t * wp = reinterpret_cast< const wchar_t * >( temp.data() );
			wdec.resize( size );

			std::copy( wp, wp + size, wdec.begin() );

			return wdec;
		}
	}
	
} }